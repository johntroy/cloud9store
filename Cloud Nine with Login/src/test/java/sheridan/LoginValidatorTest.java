package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testHasAlphaCharactersAndNumbersOnlyRegular() {
		assertTrue("Invalid case chars", LoginValidator.isValidLoginName("Troy12345"));
	}
	
	@Test
	public void testHasAlphaCharactersAndNumbersOnlyBoundaryIn() {
		assertTrue("Invalid case chars", LoginValidator.isValidLoginName("Troy1266"));
	}
	
	@Test
	public void testHasAlphaCharactersAndNumbersOnlyException() {
		assertFalse("Invalid case chars", LoginValidator.isValidLoginName("troy1244$#"));
	}
	
	@Test
	public void testHasAlphaCharactersAndNumbersOnlyBoundaryOut() {
		assertFalse("Invalid case chars", LoginValidator.isValidLoginName("troy1233#@"));
	}
	
	
	
	
	
	@Test
	public void teshasAtleastSixAlphaCharsRegular() {
		assertTrue("Invalid case chars", LoginValidator.isValidLoginName("troy5433"));
	}
	
	
	@Test
	public void testhasAtleastSixAlphaCharsBoundaryIn() {
		assertTrue("Invalid case chars", LoginValidator.isValidLoginName("troy667"));
	}
	
	@Test
	public void testhasAtleastSixAlphaCharsException() {
		assertFalse("Invalid case chars", LoginValidator.isValidLoginName("tr11"));
	}
	
	@Test
	public void testhasAtleastSixAlphaCharsBoundaryOut() {
		assertFalse("Invalid case chars", LoginValidator.isValidLoginName("tro12"));
	}
	
}

package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
	return loginName.matches("^[A-Za-z]\\w{5, 29}$");
	}
}
